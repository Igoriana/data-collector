﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using System.Linq;
using System.IO;

namespace IndeedCrauler
{
    [Flags]
    enum IndeedSearchLimits
    {
        None=0,
        JobTitles=1,
        Skills=2,
        Companies=4,
        FieldOfStudy=8,
    }
   
    enum JobTypes
    {
        All=0,
        FullTime=1,
        PartTime=2,
        Contract=4,
        Commision=8,
        Internship=16,
    }
    enum WorkExperiences
    {
        All=0,
        LessThan1Year=1,
        Years_1_2=2,
        Years_3_5=4,
        Years_6_10 = 8,
        MoreThan10Years=16,
    }
    enum Educations
    {
        All=0,
        Masters=1,
        Bachelors=2,
        Associates=4,
        Diploma=8,
        Doctorate=16
    }

    enum AgeOfResume
    {
        All=0,
        Day=1,
        Week=2,
        Month=4,
    }

    [Flags]
    enum Companies
    {
        All=0,
        SelfEmployed=1,
        MilitaryOnly=2,
    }

    enum SortBy
    {
        Relevance=0,
        Date=1,
    }

    public delegate void ResumeCallback(string url, string html);

    class IndeedSearchQueryBuilder
    {
        public string searchWhat { get; set; }
        public string searchWhere { get; set; }
        public IndeedSearchLimits limits { get; set; }
        public JobTypes jobTypes { get; set; }
        public WorkExperiences workExpiriences { get; set; }
        public Educations educations { get; set; }
        public Companies companies { get; set; }
        public AgeOfResume ageOfresume { get; set; }
        public SortBy sort { get; set; }
        public string Country { get; set; }
        public int Radius { get; set; }

        public int Start { get; set; }
        public IndeedSearchQueryBuilder()
        {
            limits = IndeedSearchLimits.JobTitles;
            Country = "US";
            Radius = 100;
            sort = SortBy.Relevance;
            Start = 0;
        }

        public IndeedSearchQueryBuilder(string What, string Where)
        {
            searchWhat = What;
            searchWhere = Where;
            limits = IndeedSearchLimits.JobTitles;
            Country = "US";
            Radius = 100;
            sort = SortBy.Relevance;
            Start = 0;
        }

        private string SearchLimitsToParams()
        {
            var ValuesOfLimits = Enum.GetValues(typeof(IndeedSearchLimits));
            List<string> lims = new List<string>();

            foreach (IndeedSearchLimits value in ValuesOfLimits)
            {
                if ((limits & value) == value)
                {
                    switch (value)
                    {
                        default:
                        case IndeedSearchLimits.None:
                            break;
                        case IndeedSearchLimits.JobTitles:
                            lims.Add("cb=jt");
                            break;
                        case IndeedSearchLimits.Skills:
                            lims.Add("cb=skills");
                            break;
                        case IndeedSearchLimits.Companies:
                            lims.Add("cb=company");
                            break;
                        case IndeedSearchLimits.FieldOfStudy:
                            lims.Add("cb=fos");
                            break;
                    }

                }
            }

            if (lims.Count > 0)
            {
                return string.Join("&", lims);
            }
            else return "";
        }


        private string JobTypeToParams()
        {
            switch (jobTypes)
            {
                default:
                case JobTypes.All: return "";
                case JobTypes.Commision: return "jtyp:cm";
                case JobTypes.Contract: return "jtyp:ct";
                case JobTypes.FullTime: return "jtyp:ft";
                case JobTypes.Internship: return "jtyp:in";
                case JobTypes.PartTime: return "jtyp:pt";
            }
        }

        private string WorkExpirienceToParams()
        {
            switch (workExpiriences)
            {
                default:
                case WorkExperiences.All: return "";
                case WorkExperiences.LessThan1Year: return "yoe:1-11";
                case WorkExperiences.MoreThan10Years: return "yoe:121";
                case WorkExperiences.Years_1_2: return "yoe:12-24";
                case WorkExperiences.Years_3_5: return "yoe:25-60";
                case WorkExperiences.Years_6_10: return "yoe:61-120";
            }
        }
        private string EducationToParams()
        {
            switch (educations)
            {
                default:
                case Educations.All: return "";
                case Educations.Associates: return "dt:as";
                case Educations.Bachelors: return "dt:ba";                
                case Educations.Diploma: return "dt:di";
                case Educations.Masters: return "dt:ms";
                case Educations.Doctorate: return "dt:doc";
            }
        }
        private string CompaniesToParams()
        {
            var ValuesOfCompanies = Enum.GetValues(typeof(Companies));
            List<string> comps = new List<string>();

            foreach (Companies value in ValuesOfCompanies)
            {
                if ((companies & value) == value)
                {
                    switch (value)
                    {
                        default:
                        case Companies.All: ;
                            break;
                        case Companies.MilitaryOnly:
                            comps.Add("mil:1");
                            break;
                        case Companies.SelfEmployed:
                            comps.Add("cmpid:13");
                            break;
                    }
                }
            }

            if (comps.Count > 0)
            {
                return string.Join(",", comps);
            }
            else return "";
        }

        private string rb()
        {
            List<string> Params = new List<string>();
            string s = JobTypeToParams();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = WorkExpirienceToParams();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = CompaniesToParams();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = EducationToParams();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            return Params.Count>0?"rb="+ WebUtility.UrlEncode(string.Join(",", Params)):"";
        }

        private string q()
        {
            return "q=" + WebUtility.UrlEncode(searchWhat);
        }

        private string l()
        {
            return "l=" + WebUtility.UrlEncode(searchWhere);
        }

        private string radius()
        {
            return "radius=" + Radius.ToString();
        }
        private string co()
        {
            if (Country != "") return "co=" + Country; else return "";
        }
        private string sorting()
        {
            return sort == SortBy.Date ? "sort=date" : "";
        }

        private string lmd()
        {
            switch (ageOfresume)
            {
                default:
                case AgeOfResume.All:return "";
                case AgeOfResume.Day: return "lmd=day";
                case AgeOfResume.Month: return "lmd=month";
                case AgeOfResume.Week: return "lmd=week";
            }
        }
    
        private string BuildParamsSection()
        {
            
            List<string> Params = new List<string>();

            string s = q();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = l();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = SearchLimitsToParams();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = rb();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = co();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = radius();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = sorting();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            s = lmd();
            if (!string.IsNullOrEmpty(s)) Params.Add(s);
            if (Start > 0) Params.Add(string.Format("start={0}", Start));
            return string.Join("&", Params);
        }

        
        public string BuildQuerySting()
        {
            string URL = BuildParamsSection();            
            return string.IsNullOrEmpty(URL)?"https://www.indeed.com/resumes": "https://www.indeed.com/resumes?"+URL;
        }
    }


    class Indeed
    {
        private HttpClient http;
        private string _login;
        private string _pass;
        private string _surftok;
        private bool _authentificated;
        private string baseurl;
        public Indeed()
        {
            http = new HttpClient();            
        }

        private bool ExtractSubstring(string content, string prefix, string suffix, out string substring)
        {
            substring = "";
            int begpos = content.IndexOf(prefix);
            if (begpos < 0) return false; else begpos += prefix.Length;
            int endpos = content.IndexOf(suffix, begpos);
            if (endpos <= begpos) return false;
            else
            {
                substring = content.Substring(begpos, endpos - begpos);
                return true;
            }
        } 

        private bool GetSurfToken(string Content)
        {
            return ExtractSubstring(Content, "surftok\\\":\\\"", "\\\"", out _surftok);
        }

        private string GetFormTk(string Content)
        {
            var formtk = "";
            ExtractSubstring(Content, "form_tk\\\":\\\"", "\\\"", out formtk);
            return formtk;
        }

        private bool IsLoggedIn(string Content)
        {
            _authentificated = !string.IsNullOrEmpty(_login) && Content.Contains(_login)&&(Content.Contains("\"loggedin\": 1")|| Content.Contains("\"loggedIn\":true"));
            return _authentificated;
        }

        public class Parameters : Dictionary<string, string>
        { }

        public async Task<string> Get(string url, Parameters parameters)
        {
            if (parameters == null) parameters = new Parameters()
            {
                {"User-Agent", "MorbaxHeadHunter" },
                {"Cache-Control", "max-age=86400" }
            };
            else
            {
                if (!parameters.ContainsKey("User-Agent")) parameters.Add("User-Agent", "MorbaxHeadHunter");
                if (!parameters.ContainsKey("Cache-Control")) parameters.Add("Cache-Control", "max-age=86400");
            }

            http.DefaultRequestHeaders.Clear();
            http.DefaultRequestHeaders.Accept.Clear();
            foreach (var p in parameters)
            {
                http.DefaultRequestHeaders.Add(p.Key, p.Value);
            }

            var responce = await http.GetAsync(url, HttpCompletionOption.ResponseContentRead);
            if (responce.IsSuccessStatusCode)
            {
                return await responce.Content.ReadAsStringAsync();
            }
            else return "";//Add callback for error logs
        }

        public async Task<string> Post(string url, Parameters parameters)
        {

            http.DefaultRequestHeaders.Clear();
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Add("User-Agent", "MorbaxHeadHunter");
            FormUrlEncodedContent requestParams = null;
            if (parameters != null)
                requestParams = new FormUrlEncodedContent(parameters);
            var responce = await http.PostAsync(url, requestParams);

            if (responce.IsSuccessStatusCode)
            {
                return await responce.Content.ReadAsStringAsync();
            }
            else return "";//Add callback for error logs
        }
        public async void Logout()
        {
            var responce=await Get("https://secure.indeed.com/account/logout?hl=en&service=my", null);            
        }

        

        private const string LoginURL = "https://secure.indeed.com/account/login?service=my";
        public async Task<bool> Login(string login, string password)
        {
            if (_authentificated && login.Trim().ToLower() == _login.Trim().ToLower() && password == _pass) return true;
            Logout();
            _login = login;
            _pass = password;

            string LoginPage = await Get("https://secure.indeed.com/account/login?service=my",null);
            if (!GetSurfToken(LoginPage)) throw new Exception("The surftok is not found!");
            var FormTk = GetFormTk(LoginPage);
            var AfterAuth = await Post("https://secure.indeed.com/account/login", 
                new Parameters()
                {
                    { "action", "login" },
                    { "surftok",_surftok },
                    {"form_tk",FormTk },
                    {"__email", login },
                    {"__password", password }
                });

            return AfterAuth.Contains(login);
        }

        const string listItemEntryOpen = "<li";
        const string listItemEntryClose = "</li>";
        const string urlprefixtag = "data-tn-element=\"resume-result-link[]\""; //признак тега, содержащего линк на резюме
        const string hreftag = "href=\"";
        private bool ParseResumesListPage(ref StringCollection hrefsList, string Content)
        {


            string results = "";
            int begpos = Content.IndexOf("class=\"resultsList\"");
            if (begpos < 0) return false;
            if (!HtmlTag.Extract(Content, 0, "class=\"resultsList\"","</ol>","<ol",out results))
            {
                return false;
            }

            begpos = results.IndexOf(listItemEntryOpen);
            bool found_another_one = false;
            int resumecount = 0;
            if (begpos < 0) return false;
            string href = "";
            string link = "";
            string listItem = "";
            do
            {
                found_another_one = false;
                if (HtmlTag.Extract(results, begpos, listItemEntryOpen, listItemEntryClose, listItemEntryOpen, out listItem))
                {
                    href = "";
                    link = "";
                    found_another_one = HtmlTag.Extract(listItem, 0, urlprefixtag, ">", "", out href);
                    if (found_another_one) begpos += listItem.Length;
                    if(found_another_one) found_another_one &= HtmlTag.ParameterValue(href, 0, "href", out link);
                    if (found_another_one)
                    {
                        string url = HtmlTag.InnerText(link);
                        hrefsList.Add(url);
                        resumecount++;
                    }
                }                               
            }
            while (found_another_one);
            return resumecount > 0;
        }

        const string beginofpaginationblock = "<div id=\"pagination\"";
        const string endofpaginationblock = "</div>";
        const string beginofnextpageblock = "class=\"confirm-nav next\"";
        const string endofnextpageblock = ">";
        private bool ProceedNextPage(string Content, out int start)
        {

            string nextPageUrl = "";
            start = 0;
            if (!HtmlTag.Extract(Content, 0, beginofpaginationblock, endofpaginationblock, "<div", out nextPageUrl)) return false;
            if (!HtmlTag.Extract(nextPageUrl, 0, beginofnextpageblock, endofnextpageblock, "", out nextPageUrl)) return false;
            if (!HtmlTag.ParameterValue(nextPageUrl, 0, "href", out nextPageUrl)) return false;
            int begpos = nextPageUrl.IndexOf("start=") + 6;
            if (begpos < 6) return false;
            string sval = "";
            while(begpos<nextPageUrl.Length && Char.IsDigit(nextPageUrl[begpos]))
            {
                sval += nextPageUrl[begpos];
                begpos++;
            }
            return int.TryParse(sval,out start);
        }
        public async Task<StringCollection> GetResumesLinks(IndeedSearchQueryBuilder Url)
        {
            StringCollection hrefsList = new StringCollection();
            string Content = "";
            int start = Url.Start;
            bool proceedNextPage = false;
            do
            {
                Url.Start = start;
                string currentUrl = Url.BuildQuerySting();
                Content = await Get(currentUrl,null);

                if (string.IsNullOrEmpty(Content))
                {
                    throw new Exception(string.Format("{0}\r\nThere is no links found", currentUrl));
                }
                proceedNextPage = ParseResumesListPage(ref hrefsList, Content) && ProceedNextPage(Content, out start);
            } while (proceedNextPage);
            return hrefsList.Count > 0 ? hrefsList : null;
        }

        const string beginofresume = "id=\"resume\"";
        const string endofresume = "</div>";
        const string div = "<div";
        public async Task<string> GetResumeRawHTML(string url)
        {
            string Content = await Get(url,null);

            //string Content = "";
            //FileStream F = new FileStream("C:\\Users\\Sender\\Documents\\Visual Studio 2015\\Projects\\IndeedCrauler\\IndeedCrauler\\bin\\Debug\\Debug.html", FileMode.Open);
            //byte[] bin = new byte[F.Length];
            //await F.ReadAsync(bin, 0, bin.Length);
            //F.Close();
            //F.Dispose();
            //Content = System.Text.Encoding.UTF8.GetString(bin);

            if (string.IsNullOrEmpty(Content))
            {
                throw new Exception(string.Format("{0}\r\nThere is no resume", Content));
            }

            string resume;
            if (!HtmlTag.Extract(Content, 0, beginofresume, endofresume, div, out resume))
            {
                throw new Exception(string.Format("{0}\r\nThis is not resume", Content));
            }
            else return resume;            
        }

        public async Task<bool> SearchResumes(IndeedSearchQueryBuilder builder, ResumeCallback onResumeFound)
        {
            if (onResumeFound == null) return false;
            string url = builder.BuildQuerySting();
            Uri uri = new Uri(url);
            baseurl = string.Format("{0}://{1}", uri.Scheme,uri.Host);
            StringCollection links = await GetResumesLinks(builder);
            if (links==null || links.Count == 0) return false;
            foreach (string link in links)
            {
                string content = await GetResumeRawHTML(baseurl+link);
                onResumeFound(link, content);
            }
            return true;
        }
    }

}

﻿using System;

namespace IndeedCrauler
{

    public class BiographyEvent
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Organization { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public DateTime? From { get; set; }
        public DateTime? Till { get; set; }
        public string Description { get; set; }

        public void ParseLocation(string Content)
        {
            int separatorpos = Content.LastIndexOf(',');
            if (separatorpos < 0) City = Content;
            else
            {
                City = Content.Substring(0, separatorpos).Trim();
                State = Content.Substring(separatorpos + 1, Content.Length - separatorpos - 1).Trim();
            }

        }

        public bool ParseDate(string Content, out DateTime Value)
        {
            Value = DateTime.MinValue;
            if (string.IsNullOrEmpty(Content)) return false;
            if (!DateTime.TryParse(Content, out Value))
            {
                throw new Exception(string.Format("\"{0}\" is cannot be parsed as datetime"));
            }
            else return true;
        }
        public void ParsePeriod(string Content)
        {
            int separatorpos = Content.IndexOf("to");
            DateTime val;
            if (separatorpos < 0)
            {                
                if (ParseDate(Content, out val))
                  From = val; else From = null;
            } else
            {
                string sfrom = Content.Substring(0, separatorpos).Trim();
                string still = Content.Substring(separatorpos + 2, Content.Length - separatorpos - 2).Trim();
                if (ParseDate(sfrom, out val))
                    From = val;else From = null;
                if (ParseDate(still, out val))
                    Till = val;else Till = null;
            }
        }
    }

    public class WorkExperienceItem: BiographyEvent
    {
        public WorkExperienceItem(string content)
        {                        
            string block;

            if (HtmlTag.Extract(content,0, "workExperience-","\"","",out block))
            {
                Id = block.Substring(0,block.Length-1);
            }else
            {
                Id = Guid.NewGuid().ToString();
            }

            if (HtmlTag.Extract(content,0, "<p class=\"work_title title\">", "</p>","<p",out block))
            {               
                Title = HtmlTag.InnerText(block);
            }
            
            if (HtmlTag.Extract(content, 0, "<div class=\"work_company\">", "</div>", "<div", out block))
            {
                string company;
                if (HtmlTag.Extract(block, 0, "<span class=\"bold\">","</span>", "<span", out company))
                {

                    Organization = HtmlTag.InnerText(company);
                }

                if (HtmlTag.Extract(block, 0, "<div class=\"inline-block\">", "</div>", "<div", out company))
                {                   
                    ParseLocation(HtmlTag.InnerText(company));
                }
            }

            if (HtmlTag.Extract(content, 0, "<p class=\"work_dates\">", "</p>", "<p", out block))

            {
                ParsePeriod(HtmlTag.InnerText(block));
            }

            if (HtmlTag.Extract(content, 0, "<p class=\"work_description\">", "</p>", "<p", out block))
            {
                Description = HtmlTag.InnerText(block);
            }
        }
    }

    public class EducationItem : BiographyEvent
    {
        public EducationItem(string content)
        {
            string block;
            if (HtmlTag.Extract(content, 0, "education-", "\"", "", out block))
            {
                Id = block.Substring(0, block.Length - 1);
            }
            else
            {
                Id = Guid.NewGuid().ToString();
            }

            if (HtmlTag.Extract(content, 0, "<p class=\"edu_title\">", "</p>", "<p", out block))
            {
                Title = HtmlTag.InnerText(block);
            }

            if (HtmlTag.Extract(content, 0, "<div class=\"edu_school\">", "</div>", "<div", out block))
            {
                string school;
                if (HtmlTag.Extract(block, 0, "<span class=\"bold\">", "</span>", "<span", out school))
                {

                    Organization = HtmlTag.InnerText(school);
                }

                if (HtmlTag.Extract(block, 0, "<div class=\"inline-block\">", "</div>", "<div", out school))
                {
                    ParseLocation(HtmlTag.InnerText(school));
                }
            }

            if (HtmlTag.Extract(content, 0, "<p class=\"edu_dates\">", "</p>", "<p", out block))

            {
                ParsePeriod(HtmlTag.InnerText(block));
            }

            if (HtmlTag.Extract(content, 0, "<p class=\"work_description\">", "</p>", "<p", out block))
            {
                Description = HtmlTag.InnerText(block);
            }
        }
    }

    public class ResumeItem
    {
        public string Contact { get; }
        public string Headline { get; }
        public string City { get; }
        public string State { get; }


    }
    
}

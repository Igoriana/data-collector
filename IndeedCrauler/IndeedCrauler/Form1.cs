﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndeedCrauler
{
    public partial class Form1 : Form
    {
        private delegate void BeforeNavigate2(object pDisp, ref dynamic url, ref dynamic Flags, ref dynamic TargetFrameName, ref dynamic PostData, ref dynamic Headers, ref bool Cancel);
        public Form1()
        {
            InitializeComponent();
            webBrowser2.GoBack();            
        }

        private static async Task<string> ProcessRequest()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic",Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "abuevich@gmail.com", "GainMoreContacts4Nfs"))));
            var stringTask = client.GetStringAsync("https://secure.indeed.com/account/login?service=my");
            var msg = await stringTask;

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
            stringTask = client.GetStringAsync("https://www.indeed.com/r/Joseph-Watkins/2134d2f14299071f");
            msg = await stringTask;
            return msg;
        }

        private /*async*/ void button1_Click(object sender, EventArgs e)
        {
            //richTextBox1.Text = await ProcessRequest();
            //Indeed Ind = new Indeed();
            //bool login = await Ind.Authenticate("abuevich@gmail.com", "GainMoreContacts4Nfs");
            //Ind.GetData();
            IndeedSearchQueryBuilder Q = new IndeedSearchQueryBuilder("sales","Chicago, IL");
            Q.Radius = 50;
            Q.jobTypes = JobTypes.All;
            Q.limits = IndeedSearchLimits.JobTitles | IndeedSearchLimits.Skills;
            Q.sort = SortBy.Date;
            Q.educations = Educations.Bachelors;
            Q.workExpiriences = WorkExperiences.Years_3_5;
            Q.companies = Companies.MilitaryOnly|Companies.SelfEmployed;
            Q.ageOfresume = AgeOfResume.Week;            
            textBox1.Text = Q.BuildQuerySting();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webBrowser2.Navigate(textBox1.Text);
        }

        private void webBrowser2_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            dynamic d = webBrowser2.ActiveXInstance;
            d.BeforeNavigate2 += new BeforeNavigate2((object pDisp,
           ref dynamic url,
           ref dynamic Flags,
           ref dynamic TargetFrameName,
           ref dynamic PostData,
           ref dynamic Headers,
           ref bool Cancel) => {
               if (url != null)
               {
                   textBox2.Text = (string)url;
               }
               if (PostData != null)
               {
                   byte[] myBytes = (byte[])PostData;
                   string post = ASCIIEncoding.ASCII.GetString(myBytes);
                   textBox2.Text += Environment.NewLine + post;
               }
               // Do some with PostData
           });
        }

        private async void btResume_Click(object sender, EventArgs e)
        {
            Indeed I = new Indeed();
            //bool authResult = await I.Login("abuevich@gmail.com", "GainMoreContacts4Nfs");
            bool authResult = true;
            if (!authResult) MessageBox.Show("Autorization failed");else
            {
                string resume = await I.GetResumeRawHTML(url.Text);
                I.Logout();
                txtResult.Text = resume;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            WorkExperienceItem E = new WorkExperienceItem(txtResult.Text);
        }


        private JobTypes JobType(int value)
        {
            switch (value)
            {
                default:
                case 0:
                    return JobTypes.All;
                case 1:
                    return JobTypes.FullTime;
                case 2:
                    return JobTypes.PartTime;
                case 3:
                    return JobTypes.Contract;
                case 4:
                    return JobTypes.Commision;
                case 5:
                    return JobTypes.Internship;

            }            
        }

        private AgeOfResume Age(int value)
        {
            switch (value)
            {
                default:
                case 0:
                    return AgeOfResume.All;
                case 1:
                    return AgeOfResume.Day;
                case 2:
                    return AgeOfResume.Week;
                case 3:
                    return AgeOfResume.Month;
            }
        }

        private WorkExperiences Exps(int value)
        {
            //All
            //Less 1 year
            //1 - 2 years
            //3 - 5 years
            //6 - 10 years
            //More than 10
            switch (value)
            {
                default:
                case 0:
                    return WorkExperiences.All;
                case 1:
                    return WorkExperiences.LessThan1Year;
                case 2:
                    return WorkExperiences.Years_1_2;
                case 3:
                    return WorkExperiences.Years_3_5;
                case 4:
                    return WorkExperiences.Years_6_10;
                case 5:
                    return WorkExperiences.MoreThan10Years;
            }            
        }

        private Educations Edu(int value)
        {
            //All
            //Masters
            //Bachelors
            //Associates
            //Diploma
            //Doctorate           
            switch (value)
            {
                default:
                case 0:
                    return Educations.All;
                case 1:
                    return Educations.Masters;
                case 2:
                    return Educations.Bachelors;
                case 3:
                    return Educations.Associates;
                case 4:
                    return Educations.Diploma;
                case 5:
                    return Educations.Doctorate;
            }            
        }

        private Companies Company(CheckedListBox list)
        {
            return (list.GetItemChecked(0) ? Companies.MilitaryOnly : Companies.All) | (list.GetItemChecked(1) ? Companies.SelfEmployed : Companies.All);
        }

        private IndeedSearchLimits Limits(bool title, bool skills, bool companies, bool FoS)
        {
            return (title ? IndeedSearchLimits.JobTitles : IndeedSearchLimits.None) |
                   (skills ? IndeedSearchLimits.Skills : IndeedSearchLimits.None) |
                   (companies ? IndeedSearchLimits.Companies : IndeedSearchLimits.None) |
                   (FoS ? IndeedSearchLimits.FieldOfStudy : IndeedSearchLimits.None);
        }

        string resumesdir = AppDomain.CurrentDomain.BaseDirectory + "\\Resumes\\";
        public void onResumeFound(string url, string content)
        {
            int begpos = url.IndexOf("/r/")+3;
            int endpos = url.IndexOf("/", begpos);
            string filename = "";
            if (begpos >= endpos) filename = DateTime.Now.ToString("yyyMMdd_HHmmssffff")+DateTime.Now.Ticks.ToString()+".html";
            else
            {
                filename = url.Substring(begpos, endpos - begpos) + ".html";                
            }
            using (FileStream FS = new FileStream(resumesdir + filename, FileMode.Create, FileAccess.Write))
            {
                byte[] bytes = Encoding.UTF8.GetBytes(content);
                FS.Write(bytes,0,bytes.Length);
            }
        }
        private async void button1_Click_1(object sender, EventArgs e)
        {
            DateTime Start = DateTime.Now;
            Directory.CreateDirectory(resumesdir);
            IndeedSearchQueryBuilder QB = new IndeedSearchQueryBuilder();
            QB.searchWhat = tbWhat.Text;
            QB.searchWhere = tbWhere.Text;
            QB.ageOfresume = Age(cbAge.SelectedIndex);
            QB.companies = Company(clbCompany);
            QB.educations = Edu(cbEdu.SelectedIndex);
            QB.jobTypes = JobType(cbJT.SelectedIndex);
            QB.limits = Limits(cbTitles.Checked, cbSkills.Checked, cbCompanies.Checked, cbFoS.Checked);

            Indeed Crauler = new Indeed();
            bool loggedIn = await Crauler.Login("abuevich@gmail.com","GainMoreContacts4Nfs");
            if (loggedIn == true)
            {
                bool result = await Crauler.SearchResumes(QB, onResumeFound);
                Crauler.Logout();
            }
            TimeSpan Cost = DateTime.Now.Subtract(Start);
            MessageBox.Show(string.Format("Data downloaded for {0} seconds", Cost.TotalSeconds));

        }

        public void GetAllSections(string filename)
        {
            lbSections.Sorted = false;
            using (FileStream FS = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] bin = new byte[FS.Length];
                FS.Read(bin, 0, bin.Length);
                string Content = System.Text.Encoding.UTF8.GetString(bin);
                string[] sections;
                if (HtmlTag.ExtractAll(Content,0, "<div class=\"section_title\">","</div>","<div",out sections))
                {
                    foreach (var s in sections)
                    {
                        string innerText = HtmlTag.InnerText(s);
                        if (!lbSections.Items.Contains(innerText)) lbSections.Items.Add(innerText);
                    }
                }
            }
            lbSections.Sorted = true;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fd = new FolderBrowserDialog())
            {
                fd.SelectedPath = AppDomain.CurrentDomain.BaseDirectory;
                DialogResult res = fd.ShowDialog();

                if (res == DialogResult.OK)
                {
                    var files = Directory.GetFiles(fd.SelectedPath,"*.html");
                    foreach (var f in files)
                    {
                        GetAllSections(f);
                    }
                }
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            lbSections.Items.Clear();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IndeedCrauler
{
    public static class HtmlTag
    {
        private static bool IsBreakLine(string Content, int begpos)
        {
            char[] closetag = { ' ', '/', '>' };
            return Content[begpos] == '<' && Content[begpos+1] == 'b' && Content[begpos+2] == 'r' && closetag.Contains(Content[begpos + 3]);
            
            
        }
        private static bool FindMarginsOfTag(string Content, int startposition, string startsubstr, string finishsubst, string skipchilds,bool wholetag, out int begpos, out int endpos )
        {
            begpos = -1;
            endpos = -1;
            if (string.IsNullOrEmpty(Content))
            {
                return false;
            }

            begpos = Content.IndexOf(startsubstr, startposition);//Начало содержимого 
            if (begpos < 0)//Нет начала в заданном диапазоне
            {
                return false; 
            }
            endpos = Content.IndexOf(finishsubst, begpos) + finishsubst.Length; //первый попавшийся            
            if (endpos< finishsubst.Length)
            {
                endpos = Content.Length - 1;
            }else
            if (skipchilds.Length > 0)
            {
                int nesteddivpos = begpos + startsubstr.Length;
                int nesteddivslevel = 1;
                do
                {
                    nesteddivpos = Content.IndexOf(skipchilds, nesteddivpos, endpos - nesteddivpos);
                    while (nesteddivpos >= 0)
                    {
                        nesteddivpos += skipchilds.Length;
                        nesteddivslevel++;
                        nesteddivpos = Content.IndexOf(skipchilds, nesteddivpos, endpos - nesteddivpos);
                    }

                    if (nesteddivslevel >= 0)
                    {
                        nesteddivpos = endpos;
                        nesteddivslevel--;
                        if (nesteddivslevel > 0) endpos = Content.IndexOf(finishsubst, endpos) + finishsubst.Length;
                        if (endpos < begpos)
                        {
                            //throw new Exception("Error occured while content parsing");
                            return false;
                        }
                    }
                } while (nesteddivslevel > 0);
            }

            if (wholetag)
            {
                if(Content[begpos]!='<')
                    for (int i = begpos;i>=0;i--)
                    {
                        begpos--;
                        if (Content[begpos] == '<' &&  !IsBreakLine(Content, begpos) ) break;
                    }
                if (Content[endpos - 1] != '>')
                {
                    endpos--;
                    for (int i = endpos; i < Content.Length; i++)
                    {
                        endpos++;
                        if (Content[endpos] == '>' && !IsBreakLine(Content, Content.LastIndexOf("<", endpos))) break;
                    }
                    endpos++;
                }
            }
            return true;
        }
        public static bool Extract(string Content, int startposition, string startsubstr, string finishsubst, string skipchilds, out string TagContent)
        {
            TagContent = "";
            /*
            if (string.IsNullOrEmpty(Content))
            {
                return false;
            }
            int begpos = Content.IndexOf(startsubstr, startposition);//Начало содержимого резюме
            if (begpos < 0)
            {                
                    return false; //throw new Exception(string.Format("{0}\r\nLeading substring not found", startsubstr));                
            }
            //Поиск окончания резюме. закрывающего div-а
            int endpos = Content.IndexOf(finishsubst, begpos) + finishsubst.Length; //первый попавшийся            
            if (skipchilds.Length > 0)
            {
                int nesteddivpos = begpos + startsubstr.Length;
                int nesteddivslevel = 1;
                do
                {
                    nesteddivpos = Content.IndexOf(skipchilds, nesteddivpos, endpos - nesteddivpos);
                    while (nesteddivpos >= 0)
                    {
                        nesteddivpos += skipchilds.Length;
                        nesteddivslevel++;
                        nesteddivpos = Content.IndexOf(skipchilds, nesteddivpos, endpos - nesteddivpos);
                    }

                    if (nesteddivslevel >= 0)
                    {
                        nesteddivpos = endpos;
                        nesteddivslevel--;
                        if (nesteddivslevel > 0) endpos = Content.IndexOf(finishsubst, endpos) + finishsubst.Length;
                        if (endpos < begpos)
                        {
                            //throw new Exception("Error occured while content parsing");
                            return false;
                        }
                    }
                } while (nesteddivslevel > 0);
            }
            */
            int begpos = -1;
            int endpos = -1;
            if (FindMarginsOfTag(Content, startposition, startsubstr, finishsubst, skipchilds, true, out begpos, out endpos))
            {
                TagContent = Content.Substring(begpos, endpos - begpos);
            }
            return true;
        }

        public static bool ExtractAll(string Content, int startposition, string startsubstr, string finishsubst, string skipchilds, out string[] TagsContent)
        {
            int begpos = startposition;
            int endpos = -1;
            int foundcount = 0;
            List<string> temp = new List<string>();
            while (FindMarginsOfTag(Content, begpos, startsubstr, finishsubst, skipchilds, true, out begpos, out endpos))
            {
                foundcount++;
                temp.Add(Content.Substring(begpos, endpos - begpos));
                begpos = endpos + 1;
            }
            TagsContent = temp.ToArray();
            return foundcount > 0;
        }
        public static string InnerText(string Content)
        {
            return HttpUtility.HtmlDecode(StripHTML(Content.ToString())).Trim();
        }

        public static bool ParameterValue(string Content, int startpos, string paramName, out string paramValue)
        {
            paramValue = "";
            if (string.IsNullOrEmpty(Content)) return false;
            int begpos = Content.IndexOf(paramName, startpos);            
            if (begpos < 0) return false;
            begpos += paramName.Length;
            if (begpos >= Content.Length) return true;
            if (Content[begpos] == '=')
            {
                begpos = Content.IndexOf("\"", begpos);
                int endpos = Content.IndexOf("\"", begpos + 1);
                if (begpos > endpos) return false;
                paramValue = Content.Substring(begpos + 1, endpos - begpos - 1);
            }
            return true;
        }

        private static string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {
                return source;
            }
        }

    }
}
